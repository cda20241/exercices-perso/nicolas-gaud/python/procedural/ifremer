.. Projet 3 documentation master file, created by
   sphinx-quickstart on Mon Oct  9 13:52:05 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Projet 3's documentation!
====================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   ./modules

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
