MONTHS = ['janvier', 'février', 'mars', 'avril', 'mai', 'juin', 'juillet', 'août', 'septembre', 'octobre', 'décembre']

def format_date(date):
    """
    This function formats a date from YYYY-MM-DD-HH to "DD month YYYY à HH h"

    :param date: the date to be formatted

    :return: the date formatted into a literal string
    """
    formatted_date = date[8:10] + ' ' + MONTHS[int(date[5:7]) - 1] + ' ' + date[:4] + ' à ' + date[11:] + ' h'
    return formatted_date
