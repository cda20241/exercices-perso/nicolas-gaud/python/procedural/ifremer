import unittest
from fonctions import check_coef
from convertisseur import json_to_dict
DATA = {
	"2022-01-01-02": [[1, 1, 1], [0, 0, 0], [0, 0, 0]],
    "2022-01-01-10": [[1, 1, 1], [0, 1, 1], [0, 0, 0]],
    "2022-01-01-18": [[1, 1, 1], [0, 1, 0], [0, 0, 0]],
    "2022-01-01-04": [[1, 1, 0], [0, 0, 0], [0, 0, 0]],
    "2022-01-01-12": [[1, 1, 1], [1, 1, 1], [0, 0, 1]]
}

class TestCheckCoef(unittest.TestCase):
    # Nicolas GAUD
    def test_highest_coef(self):
        """
        This function ensures that check_coef() under condition = 1 returns the date of the highest coefficient
        """
        self.assertEqual(check_coef(DATA, 1), "2022-01-01-12")

    def test_lowest_coef(self):
        """
        This function ensures that check_coef() under condition = 0 returns the date of the lowest coefficient
        """
        self.assertEqual(check_coef(DATA, 0), "2022-01-01-04")


if __name__ == '__main__':
    unittest.main()
