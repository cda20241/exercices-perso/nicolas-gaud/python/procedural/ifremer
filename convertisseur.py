import pandas as pd
import json

def convertir_xls(excel_file, csv_file, json_file):
    """
    Convertit un fichier Excel xls et xlsx en CSV et en JSON.
    cette fonction prend un fichier xls ou xlsx pour le convertir
    au format  de fichier csv puis le fichier csv en fichier json

    :param excel_file: Chemin du fichier Excel en entrée.
    :param csv_file: Chemin du fichier CSV de sortie.
    :param json_file: Chemin du fichier JSON de sortie.

    """
    #on passe en lecture du fichier xls / xlsx
    df_excel = pd.read_excel(excel_file, engine='openpyxl')
    #on converti le fichier xls en csv
    df_excel.to_csv(csv_file, index=False)
    # on passe en lecture du fichier xcsv
    df_csv = pd.read_csv(csv_file)
    # on converti le fichier csv en json
    df_csv.to_json(json_file, orient='records', indent=4)



def json_to_dict():
    """
    Cette fonction trie les données du fichier JSON en regroupant les valeurs de la colonne "Actif" par date.

    :return: Dictionnaire trié où les clés sont les dates et les valeurs sont des listes de valeurs de "Actif" en sous-listes de 3 éléments.
    :rtype: dict
    """
    # Chemin du fichier JSON prédéfini
    json_file = "marees_v1.json"

    with open(json_file, 'r', encoding='utf-8') as json_file:
        data = json.load(json_file)

    sorted_data = {}

    for item in data:
        date = item["Date"]
        actif_value = item["Actif"]

        if date not in sorted_data:
            sorted_data[date] = []

        if len(sorted_data[date]) == 0 or len(sorted_data[date][-1]) == 3:
            sorted_data[date].append([])

        sorted_data[date][-1].append(actif_value)

    return sorted_data



